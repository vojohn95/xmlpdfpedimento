<?php
use GuzzleHttp\Client;
//declare(strict_types=1);
include 'conexionEBS/conexionProd.php';
include 'composer/vendor/autoload.php';
use \CfdiUtils\XmlResolver\XmlResolver;
use \CfdiUtils\CadenaOrigen\DOMBuilder;

echo "<h1>Consulta al ebs</h1>";
echo "<br>";
echo "<h2>Consulta 1</h2>";
$stid = oci_parse($conn, 'select distinct(aps.TRX_NUMBER) "FOLIO",

ra.CUSTOMER_TRX_ID,

hc.ACCOUNT_NUMBER "CLIENTE",

aps.amount_due_original "TOTAL FACTURA",

rl.TAX_RECOVERABLE "IMPUESTO",

ent.name "ENTIDAD_ LEGAL",

reg.REGISTRATION_NUMBER "RFC EMISOR",

batc.ATTRIBUTE1 "SERIE",

rl.LINE_RECOVERABLE "SUBTOTAL DE LA FACTURA",

decode(rt.TYPE,\'INV\',\'I\',\'CM\',\'E\',rt.TYPE) "TIPO COMPROBANTE",

ra.ATTRIBUTE11 "FORMA DE PAGO",

decode(aps.class,\'INV\',\'FACTURA\',\'CM\',\'NOTA DE CREDITO\',rt.TYPE) "CLASE",

\'INMEDIATO\' "CONDICIONES DE PAGO",

ra.ATTRIBUTE10 "METODO DE PAGO",

TO_CHAR(ra.CREATION_DATE,\'YYYY/MM/DD HH:MI:SS\') "FECHA DE EMISION",

HR_LOC.POSTAL_CODE "LUGAR DE EXPEDICION",

hp.party_name "RECEPTOR",

hp.JGZZ_FISCAL_CODE "RECEPTOR RFC",

ra.ATTRIBUTE6 "USO CFDI",

ra.INVOICE_CURRENCY_CODE "MONEDA",

rl.DESCRIPTION "DESCRIPCION FACTURA",

inv.SEGMENT1 "CLAVE PRODUCTO SERVICIO",

inv.SEGMENT2 "CLAVE PRODUCTO SAT",

inv.SEGMENT3 "CLAVE PRODUCTO DESCRIPCION",

ra.ATTRIBUTE13 "TIPO DE RELACION",inv.INVENTORY_ITEM_ID "NUMERO DE IDENTIFICACION",

inv.PRIMARY_UNIT_OF_MEASURE "UNIDAD",

rl.GROSS_UNIT_SELLING_PRICE "VALOR UNITARIO",

rl.QUANTITY_CREDITED "CANTIDAD",

rl.TAX_RECOVERABLE "Importe del impuesto",

ra.ATTRIBUTE12 "UUID-FACTURA A CANCELAR",

ra.GLOBAL_ATTRIBUTE30 "UUID-NOTA CREDITO",

ra.COMMENTS "COMENTARIOS"

from ar_payment_schedules_all aps,

hz_cust_accounts hca,

ra_customer_trx_lines_all rl,

hz_cust_accounts hc,

ra_customer_trx_all ra,

xle_entity_profiles ent,

xle_registrations reg,

RA_BATCH_SOURCES_all batc,

ra_cust_trx_types_all rt,

hz_parties hp,

MTL_SYSTEM_ITEMS_B inv,

hr_locations_all hr_loc

where 1 = 1

AND hp.party_id = hca.party_id

AND aps.customer_id = hca.cust_account_id

AND ra.customer_trx_id = rl.customer_trx_id

AND ra.customer_trx_id = aps.customer_trx_id

and ent.LEGAL_ENTITY_ID = ra.LEGAL_ENTITY_ID

AND ent.LEGAL_ENTITY_ID = reg.source_id

and batc.BATCH_SOURCE_ID = ra.BATCH_SOURCE_ID

AND ra.cust_trx_type_id = rt.cust_trx_type_id

AND reg.location_id = hr_loc.location_id

AND rl.INVENTORY_ITEM_ID = inv.INVENTORY_ITEM_ID

AND ra.bill_to_customer_id = hc.cust_account_id

--AND hc.status = \'A\'

AND hp.party_id = hc.party_id

--AND RECEIPT_REQUIRED_FLAG = \'Y\'

--AND TO_CHAR(ra.creation_date ,\'YYYY/MM/DD\') BETWEEN \'2019/04/01\' AND \'2019/04/30\'

--and aps.class = \'CM\'

and batc.ATTRIBUTE1  = \'M\'

--AND batc.ATTRIBUTE1 IS NOT NULL

and hca.account_number = \'9974\'

AND aps.TRX_NUMBER IN (\'4856\', \'\')

--and inv.PRIMARY_UNIT_OF_MEASURE like \'%0027%\'

--AND reg.REGISTRATION_NUMBER = \'SCE940913BX0\'

ORDER BY FOLIO');
if (!$stid) {
    $e = oci_error($conn);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}
// Realizar la lógica de la consulta
oci_execute($stid);

$ArrayCon = array(

);

print "<table border='1'>\n";
while ($fila = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    print "<tr>\n";
    foreach ($fila as $elemento) {
        print "    <td>" . ($elemento !== null ? htmlentities($elemento, ENT_QUOTES) : "") . "</td>\n";
        array_push($ArrayCon, $elemento
        );
    }
    print "</tr>\n";
}
print "</table>\n";

echo "<pre>";
var_dump($ArrayCon);
echo $folio = $ArrayCon[0];
echo $total = $ArrayCon[3];
echo $fecha = $ArrayCon[10];
echo $receptor = $ArrayCon[11];
echo $rfc = $ArrayCon[12];
$total = '11194';
$iva = '1544';
$subtotal = $total - $iva;
echo "</pre>";
echo "<br>";

$certificado = new \CfdiUtils\Certificado\Certificado('sellos/00001000000402646722.cer');
$comprobanteAtributos = [
    'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
    'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
    'LugarExpedicion' => '01020',
    'MetodoPago' => 'PPD',
    'TipoDeComprobante' => 'I',
    'Total' => $total,
    'TipoCambio' => 1,
    'Moneda' => 'MXN',
    'SubTotal' => $subtotal,
    'CondicionesDePago' => 'INMEDIATO',
    'FormaPago' => '99',
    'Fecha' => '2017-11-27T01:29:18',
    'Folio' => $folio,
    'Serie' => 'M',
    'Version' => '3.3',
    'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
];
$creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);
$comprobante = $creator->comprobante();

$comprobante->addEmisor([
    'RegimenFiscal' => '601',
    'Nombre' => 'Operadora Central de Estacionamientos, SAPI DE C.V.',
    'Rfc' => 'OCE9412073L3',
]);
$comprobante->addReceptor([
    'UsoCFDI' => 'G03',
    'Nombre' => $receptor,
    'Rfc' => $rfc,
]);
$concepto = $comprobante->addConcepto([
    'ClaveProdServ' => '95121644',
    'Cantidad' => '1.00',
    'ClaveUnidad' => 'E48',
    'Unidad' => 'SERVICIO',
    'Descripcion' => 'Plaza Mia Metepec',
    'ValorUnitario' => str_replace(",", "", number_format($subtotal, 2)),
    'Importe' => str_replace(",", "", number_format($subtotal, 2)),
]);
$concepto->addTraslado([
    'Importe' => str_replace(",", "", number_format($iva, 2)),
    'TasaOCuota' => '0.160000',
    'TipoFactor' => 'Tasa',
    'Impuesto' => '002',
    'Base' => str_replace(",", "", number_format($subtotal, 2)),
]);
$creator->addSumasConceptos(NULL, 2);
echo "<h1>Paso 1</h1>";
$key = file_get_contents('sellos/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20160520_091056.key.pem');
echo "<h1>Paso 2</h1>".$key;
$creator->addSello($key, 'oce94120');
echo "<h1>Paso 3</h1>";
$creator->saveXml('cristofer.xml');
echo "<h1>Paso 4</h1>";
$xml = $creator->asXml();
echo "<br>";
echo "guardo la factura";


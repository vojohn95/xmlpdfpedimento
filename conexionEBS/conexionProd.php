<?php
// Conectar al servicio Oracle Database
// syntaxis: oci_connect(usuario oracle, clave, servidor/nombre servicio)
$conn = oci_connect('APPS', 'pr0dx10n',
    '(DESCRIPTION = (
            ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.0.203)(PORT = 1536))
            )
            (CONNECT_DATA = (SERVICE_NAME = OCEPROD)
            )
        )'
);
//Comprobacion de conexion

if(!$conn){
    $e = oci_error();
    echo "Error en la conexion".$e;
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USSER_ERROR);
}else{
    //echo "Conexion satisfactoria a ebs<br>";
}

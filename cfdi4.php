<?php
use GuzzleHttp\Client;
//declare(strict_types=1);
include 'composer/vendor/autoload.php';
use \CfdiUtils\XmlResolver\XmlResolver;
use \CfdiUtils\CadenaOrigen\DOMBuilder;
use \CfdiUtils\Elements\Cfdi40;
use \CfdiUtils\CfdiCreator40;

//se tiene que dejar habilitado un select para el objimpuesto se pede escoger y en se debe habilitar el catalogo de regimenes fiscales para receptor

$certificado = new \CfdiUtils\Certificado\Certificado('sellos/00001000000503977924.cer');
$comprobanteAtributos = [
    'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/4',
    'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
    'LugarExpedicion' => '01020',
    'MetodoPago' => 'PUE',
    'TipoDeComprobante' => 'I',
    'Exportacion' => '01',
    'Total' => '2399',
    'TipoCambio' => 1,
    'Moneda' => 'MXN',
    'SubTotal' => '2143.03',
    'CondicionesDePago' => 'INMEDIATO',
    'FormaPago' => '99',
    'Fecha' => '2022-02-22T01:29:18',
    'Folio' => '000249',
    'Serie' => 'M',
    'Version' => '4.0',
    'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd',
];
$creator = new \CfdiUtils\CfdiCreator40($comprobanteAtributos, $certificado);
$comprobante = $creator->comprobante();

$comprobante->addEmisor([
    'RegimenFiscal' => '601',
    'Nombre' => 'Operadora Central de Estacionamientos, SAPI DE C.V.',
    'Rfc' => 'OCE9412073L3',
]);
$comprobante->addReceptor([
    'UsoCFDI' => 'G03',
    'Nombre' => 'Luis Fernando Jonathan Vargas Osornio',
    'Rfc' => 'VAOL9504286P9',
    'DomicilioFiscalReceptor' => '04200',
    'RegimenFiscalReceptor' => '621',
]);
$concepto = $comprobante->addConcepto([
    'ClaveProdServ' => '95121644',
    'Cantidad' => '1.00',
    'ClaveUnidad' => 'E48',
    'Unidad' => 'SERVICIO',
    'Descripcion' => 'Plaza Mia Metepec',
    'ValorUnitario' => '2399',
    'Importe' => '2399',
    'ObjetoImp' => "02",
]);
$concepto->addTraslado([
    'Importe' => '255.97',
    'TasaOCuota' => '0.160000',
    'TipoFactor' => 'Tasa',
    'Impuesto' => '002',
    'Base' => '2399',
]);
$creator->addSumasConceptos(NULL, 2);
echo "<h1>Paso 1</h1>";
$key = file_get_contents('sellos/CSD_UNIDAD_OCE9412073L3_20200514_111724.key.pem');
echo "<h1>Paso 2</h1>".$key;
$creator->addSello($key, 'OCE94120');
echo "<h1>Paso 3</h1>";
$creator->saveXml('cfdi4.xml');
echo "<h1>Paso 4</h1>";
$xml = $creator->asXml();
echo "<br>";
echo "guardo la factura";


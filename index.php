<?php
use GuzzleHttp\Client;
//declare(strict_types=1);
include 'conexionEBS/conexionProd.php';
include 'composer/vendor/autoload.php';
use \CfdiUtils\XmlResolver\XmlResolver;
use \CfdiUtils\CadenaOrigen\DOMBuilder;
// Preparar la sentencia
$fechadehoy = date("Y-m-d");
$directorio = "facturas/".$fechadehoy;

if (file_exists($directorio)) {
echo "El directorio existe";
} else {
echo "El directorio no existe";
mkdir("facturas/".$fechadehoy);
echo "Se creo carpeta";
}

echo "<h1>Consulta al ebs</h1>";
echo "<br>";
echo "<h2>Consulta 1</h2>";
$stid = oci_parse($conn, 'SELECT distinct(RA.TRX_NUMBER)               "FOLIO",
       hc.ACCOUNT_NUMBER "CLIENTE",
       rl.LINE_NUMBER                       "NUMERO LINEA",
       aps.amount_due_original              "TOTAL DE LA FACTURA",
       rl.TAX_CLASSIFICATION_CODE           "IMPUESTO",
       ent.name                             "ENTIDAD_ LEGAL",
       reg.REGISTRATION_NUMBER              "RFC EMISOR",
       batc.ATTRIBUTE1                      "SERIE",
       rl.LINE_RECOVERABLE                  "SUBTOTAL DE LA FACTURA",
       decode(rt.TYPE,\'INV\',\'I\',\'CM\',\'E\',rt.TYPE) "TIPO COMPROBANTE",
       ra.ATTRIBUTE11                       "FORMA DE PAGO",
       ter.NAME                             "CONDICIONES DE PAGO",
       ra.ATTRIBUTE10                       "METODO DE PAGO",
       TO_CHAR(ra.CREATION_DATE,\'YYYY/MM/DD HH:MI:SS\')  "FECHA DE EMISION",
       HR_LOC.POSTAL_CODE                               "LUGAR DE EXPEDICION",
       hp.party_name                                    "RECEPTOR",
       hp.JGZZ_FISCAL_CODE                              "RECEPTOR RFC",
       ra.ATTRIBUTE6                                    "USO CFDI",
       ra.INVOICE_CURRENCY_CODE                         "MONEDA",
       rl.DESCRIPTION                                   "DESCRIPCION FACTURA",
       inv.SEGMENT1                                     "CLAVE PRODUCTO SERVICIO",
       inv.SEGMENT2                                     "CLAVE PRODUCTO SAT",
       inv.SEGMENT3                                     "CLAVE PRODUCTO DESCRIPCION",
       ra.GLOBAL_ATTRIBUTE30                            "UUID-FACTURA",
       inv.INVENTORY_ITEM_ID                            "NUMERO DE IDENTIFICACION",
       inv.PRIMARY_UNIT_OF_MEASURE                      "UNIDAD",
       rl.GROSS_UNIT_SELLING_PRICE                      "VALOR UNITARIO",
       rl.QUANTITY_INVOICED                             "CANTIDAD",
       rl.TAX_RECOVERABLE                               "Importe del impuesto",
       ra.COMMENTS                                      "COMENTARIOS",
       rt.NAME                                          "PROYECTO",
       rt.DESCRIPTION                                   "DESCRIPCION PROYECTO",
       ra.GLOBAL_ATTRIBUTE30                            "UUID SAT"
FROM ra_customer_trx_all ra,
ra_customer_trx_lines_all rl,
ar_payment_schedules_all aps,
ra_cust_trx_types_all rt,
hz_cust_accounts hc,
hz_parties hp,
hz_cust_acct_sites_all hcasa_bill,
hz_cust_site_uses_all hcsua_bill,
hz_party_sites hps_bill,
ra_cust_trx_line_gl_dist_all rct,
MTL_SYSTEM_ITEMS_B           inv,
RA_BATCH_SOURCES_all           batc,
RA_TERMS_TL                   ter,
xle_entity_profiles           ent,
xle_registrations              reg,
hr_locations_all               hr_loc
WHERE 1 = 1
AND ra.customer_trx_id = rl.customer_trx_id
AND ra.customer_trx_id = aps.customer_trx_id
AND ra.org_id = aps.org_id
AND rct.customer_trx_id = aps.customer_trx_id
AND rct.customer_trx_id = ra.customer_trx_id
AND rct.customer_trx_id = rl.customer_trx_id
AND rct.customer_trx_line_id = rl.customer_trx_line_id
and batc.BATCH_SOURCE_ID = ra.BATCH_SOURCE_ID
AND ra.complete_flag = \'Y\'
AND rl.line_type IN (\'FREIGHT\', \'LINE\')
AND ra.cust_trx_type_id = rt.cust_trx_type_id
AND ra.bill_to_customer_id = hc.cust_account_id
AND hc.status = \'A\'
AND hp.party_id = hc.party_id
AND hcasa_bill.cust_account_id = ra.bill_to_customer_id
AND hcasa_bill.cust_acct_site_id = hcsua_bill.cust_acct_site_id
AND hcsua_bill.site_use_code = \'BILL_TO\'
AND hcsua_bill.site_use_id = ra.bill_to_site_use_id
AND hps_bill.party_site_id = hcasa_bill.party_site_id
AND hcasa_bill.status = \'A\'
AND hcsua_bill.status = \'A\'
AND rl.INVENTORY_ITEM_ID = inv.INVENTORY_ITEM_ID
AND ra.TERM_ID  = ter.TERM_ID
and ent.LEGAL_ENTITY_ID  = ra.LEGAL_ENTITY_ID
AND ent.LEGAL_ENTITY_ID  = reg.source_id
AND reg.location_id      =  hr_loc.location_id
AND RECEIPT_REQUIRED_FLAG = \'Y\'
AND batc.NAME NOT IN (\'E_CENTRAL_EXN\',\'E_CENTRAL\')AND inv.ORGANIZATION_ID = \'121\'AND batc.ATTRIBUTE1 IN (\'M\',\'H\',\'I\',\'K\')  AND ra.TRX_NUMBER = \'13027\'
  AND hc.ACCOUNT_NUMBER = \'9914\'
AND reg.REGISTRATION_NUMBER = \'OCE9412073L3\'
ORDER BY FOLIO');
if (!$stid) {
    $e = oci_error($conn);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}
// Realizar la lógica de la consulta
$r = oci_execute($stid);
$array  = array();



        $fecha = "2019-07-29";
        $fecha2 = '10:06:53';//date('H:i:s');
        $certificado = new \CfdiUtils\Certificado\Certificado('sellos/00001000000402646722.cer');
        $comprobanteAtributos = [
            'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'LugarExpedicion' => '01020',
            'MetodoPago' => 'PPD',
            'TipoDeComprobante' => 'I',
            'Total' => '9070593.24',
            'TipoCambio' => 1,
            'Moneda' => 'MXN',
            'SubTotal' => '7819476.96',
            'CondicionesDePago' => 'INMEDIATO',
            'FormaPago' => '99',
            'Fecha' => $fecha . "T" . $fecha2,
            'Folio' => '13192',
            'Serie' => 'H',
            'Version' => '3.3',
            'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
        ];
        $creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);
        $comprobante = $creator->comprobante();

        $comprobante->addEmisor([
              'RegimenFiscal' => '601',
              'Nombre' => 'Operadora Central de Estacionamientos, SAPI DE C.V.',
            'Rfc' => 'OCE9412073L3',
        ]);
        $comprobante->addReceptor([
            'UsoCFDI' => 'I08',
            'Nombre' => 'ENGENCAP HOLDING S DE RL DE CV',
            'Rfc' => 'GCC960325DK7',
        ]);
        /*$relacion = $comprobante->addCfdiRelacionados([
          'TipoRelacion' => '04'
        ]);
        $relacion->addCfdiRelacionado([
          'UUID' => '0dac3428-656d-4434-b9f7-f22c65ddc7d3',
        ]);*/


while (oci_fetch($stid)) {
  $FOLIO = oci_result($stid, 'FOLIO');
  $cliente = oci_result($stid, 'CLIENTE');
  $nl = oci_result($stid, 'NUMERO LINEA');
  $TotalFact = oci_result($stid, 'TOTAL DE LA FACTURA');
  $Impuesto = oci_result($stid, 'IMPUESTO');
  $Entidad_legal = oci_result($stid, 'ENTIDAD_ LEGAL');
  $rfc_emisor = oci_result($stid, 'RFC EMISOR');
  $serie = oci_result($stid, 'SERIE');
  $subT_fact = oci_result($stid, 'SUBTOTAL DE LA FACTURA');
  $t_comprobante = oci_result($stid, 'TIPO COMPROBANTE');
  $f_pago = oci_result($stid, 'FORMA DE PAGO');
  $c_pago = oci_result($stid, 'CONDICIONES DE PAGO');
  $m_pago = oci_result($stid, 'METODO DE PAGO');
  $f_emision = oci_result($stid, 'FECHA DE EMISION');
  $l_exp = oci_result($stid, 'LUGAR DE EXPEDICION');
  $receptor = oci_result($stid, 'RECEPTOR');
  $receptor_rfc = oci_result($stid, 'RECEPTOR RFC');
  $usoCFDI = oci_result($stid, 'USO CFDI');
  $moneda = oci_result($stid, 'MONEDA');
  $desc_fact = oci_result($stid, 'DESCRIPCION FACTURA');
  $uuid_fact = oci_result($stid, 'UUID-FACTURA');
  $clave_Proser = oci_result($stid, 'CLAVE PRODUCTO SERVICIO');
  $clave_sat = oci_result($stid, 'CLAVE PRODUCTO SAT');
  $clav_Prodesc = oci_result($stid, 'CLAVE PRODUCTO DESCRIPCION');
  $no_id = oci_result($stid, 'NUMERO DE IDENTIFICACION');
  $unidad = oci_result($stid, 'UNIDAD');
  $val_uni = oci_result($stid, 'VALOR UNITARIO');
  $cantidad = oci_result($stid, 'CANTIDAD');
  $importe_imp = oci_result($stid, 'Importe del impuesto');
  $comentarios = oci_result($stid, 'COMENTARIOS');
  $Proyecto = oci_result($stid, 'PROYECTO');
  $desc_proy = oci_result($stid, 'DESCRIPCION PROYECTO');
  //$t_relacion = oci_result($stid, 'TIPO DE RELACION');
  //$uuid_cancelar = oci_result($stid, 'UUID-FACTURA A CANCELAR');
  $uuid_sat = oci_result($stid, 'UUID SAT');

  $ArrayCon = array(

  );
  array_push($ArrayCon, $FOLIO, $cliente, $nl, $TotalFact, $Impuesto, $Entidad_legal, $rfc_emisor, $serie,
  $subT_fact, $t_comprobante, $f_pago, $c_pago, $m_pago, $l_exp, $receptor_rfc, $usoCFDI, $moneda, $desc_fact,
  $uuid_fact, $clave_Proser, $clave_sat, $clav_Prodesc, $no_id, $unidad, $val_uni, $cantidad, $importe_imp, $comentarios,
  $Proyecto, $desc_proy, $uuid_sat
 );
  array_push($array,$ArrayCon);
  echo "<pre>";
  print_r($array);
  echo "</pre>";
  echo "<br>";

  //$descripcion = str_replace("18 43 3079 8000226", "19  43  3709  9000284", $desc_fact);
  $concepto = $comprobante->addConcepto([
      'ClaveProdServ' => '72153700',
      'NoIdentificacion' => '2622',
      'Cantidad' => '1.00',
      'ClaveUnidad' => 'H87',
      'Unidad' => $unidad,
      'Descripcion' => $desc_fact,
      'ValorUnitario' => str_replace(",", "", number_format($subT_fact, 2)),
      'Importe' => str_replace(",", "", number_format($subT_fact, 2)),
  ]);
  $concepto->addTraslado([
      'Importe' => str_replace(",", "", number_format($importe_imp, 2)),
      'TasaOCuota' => '0.160000',
      'TipoFactor' => 'Tasa',
      'Impuesto' => '002',
      'Base' => str_replace(",", "", number_format($subT_fact, 2)),
  ]);
  /*$concepto->addInformacionAduanera([
    'NumeroPedimento' => '19  43  3709  9000284'
  ]);*/


}
//echo('<pre>');
//print_r($array);
//echo('</pre>');
echo "<br>";
echo "<hr>";
/*if (!$r) {
    $e = oci_error($stid);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
*/

// Obtener los resultados de la consulta
print "<table border='1'>\n";
while ($fila = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    print "<tr>\n";
    foreach ($fila as $elemento) {
        print "    <td>" . ($elemento !== null ? htmlentities($elemento, ENT_QUOTES) : "") . "</td>\n";
    }
    print "</tr>\n";
}
print "</table>\n";

/*
        $creator->addSumasConceptos(NULL, 2);
        echo "<h1>Paso 1</h1>";
        $key = file_get_contents('sellos/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20160520_091056.key.pem');
        echo "<h1>Paso 2</h1>".$key;
        $creator->addSello($key, 'oce94120');
        echo "<h1>Paso</h1>";
        $creator->saveXml('nose.xml');
        echo "<h1>Paso</h1>";
        $xml = $creator->asXml();
        echo "<h1>Paso</h1>";
        $xml2 = base64_encode($xml);
        echo "<h1>Paso</h1>";
        $body = array(
            'xml' => $xml2,
        );
echo "<h1>Paso</h1>";
*/
/*
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-stage.emite.dev/v1/stamp",//desarrollo
            //CURLOPT_URL => "https://api.emite.app/v1/stamp",//produccion
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json; charset=UTF-8",
                "Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM",//desarrollo
                //"Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI",//produccion
                "Content-Type: application/json; charset=UTF-8"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return $err;
        } else {
            $val = json_decode($response);
            echo $response;
          }
        if ($creator){
          echo "<h3>Se creo archivo</h3>";
        }else{
          echo "<h1>Fallo al crear</h1>";
        }
        $xml3 = $valor->data->document_info->xml;
                        $uuid = $val->data->stamp_info->uuid;
                        $xml3 = base64_decode($xml3);
                        file_put_contents($uuid . ".xml", $xml3);

$cfdifile = $uuid . ".xml";
$xml = file_get_contents($cfdifile);

// clean cfdi
$xml = \CfdiUtils\Cleaner\Cleaner::staticClean($xml);

// create the main node structure
$comprobante = \CfdiUtils\Nodes\XmlNodeUtils::nodeFromXmlString($xml);

// create the CfdiData object, it contains all the required information
$cfdiData = (new \PhpCfdi\CfdiToPdf\CfdiDataBuilder())
    ->build($comprobante);

// create the converter
$converter = new \PhpCfdi\CfdiToPdf\Converter(
    new \PhpCfdi\CfdiToPdf\Builders\Html2PdfBuilder()
);

// create the invoice as output.pdf
$converter->createPdfAs($cfdiData, $uuid . ".pdf");

if ($converter) {
  echo "<h1>PDF generado</h1>";
}else{
  echo "<h1>Error al generar pdf</h1>";
}

$origenXml = $uuid . ".xml";
$origenPdf = $uuid . ".pdf";
$destino = "facturas/".$fechadehoy."/";
var_dump($destino);

move_to($origenXml,$destino);
move_to1($origenPdf,$destino);

function move_to($origenXml,$destino){
  copy($origenXml,$destino);
  //unlink($origenXml);
  return "Se movio xml a la carpeta \n";
}

function move_to1($origenPdf,$destino){
  copy($origenPdf,$destino);
  //unlink($origenPdf);
  return "Se movio pdf a la carpeta \n";

}
*/

oci_free_statement($stid);
oci_close($conn);
?>

<?php
include 'composer/vendor/autoload.php';

use \CfdiUtils\XmlResolver\XmlResolver;
use \CfdiUtils\CadenaOrigen\DOMBuilder;


function eliminar_simbolos($string)
{

    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    $string = str_replace(
        array(
            "\\", "¨", "º", "-", "~",
            "#", "@", "|", "!", "\"",
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "[", "^", "<code>", "]",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            ".", " "
        ),
        ' ',
        $string
    );
    return $string;
}


// $reader = new PhpOffice\PhpSpreadsheet\Reader\Xlsx();
// $excel = $reader->load('engeniummarzo.xlsx');
// //$excel = $reader->load('FACTURAENGENIUM1_11.xlsx');
// $excel->getSheetCount();
// $excelData = $excel->getSheet(0)->toArray();
// $res = array();
// foreach ($excelData as $key => $value) {
//     if ($value[0] !== null || $value[1] !== null) {
//         if ($value[0] != 'Nro') {
//             $valor = [
//                 'nro' => $value[0],
//                 'art' => $value[1],
//                 'des' => $value[2],
//                 'umd' => $value[3],
//                 'CANTIDAD' => $value[4],
//                 'precio_unit' => $value[5],
//                 'importe' => $value[6],
//                 'numero' => $value[8],
//             ];
//             array_push($res, $valor);
//         }
//     }
// }

//var_dump($res);
// echo "<h1>termina lectura de excel</h1>";
/*echo "<h2>entity</h2>";
libxml_disable_entity_loader(false); // do NOT DISABLE entity loader ... means ENABLE entity loader
$path = 'composer/vendor/eclipxe/cfdiutils/build/resources//www.sat.gob.mx/sitio_internet/cfd/3/cadenaoriginal_3_3/cadenaoriginal_3_3.xslt';
$d = new DOMDocument();
$result = $d->load($path);
var_dump($result);
echo "<h2>termina var_dump</h2>";*/
// echo "<br>";

$jayParsedAry = [
    [
        "DESCRIPCIONFACTURA" => "SKU 0203051 BILLETERO IVIZION UNIVERSAL",
        "NUMERODESERIE" => " 0203051",
        "DESCRIPCIONFACTURA_1" => "SKU 0203051 BILLETERO IVIZION UNIVERSAL",
        "CLAVEPRODUCTOSERVICIO" => "EQUIPO",
        "CLAVEPRODUCTOSAT" => "72153700",
        "CLAVEPRODUCTODESCRIPCION" => "Servicios de instalación, mantenimiento",
        "NUMERODEIDENTIFICACION" => "47",
        "UNIDAD" => "SERVICIO",
        "VALORUNITARIO" => "27600",
        "cantidad" => "2",
        "Importedelimpuesto" => "0",
        "INCOTERM" => "DDP"
    ],
    [
        "DESCRIPCIONFACTURA" => "SKU 0203081 FUENTE DE IMPRESORA ATM ITAUTEC CX3",
        "NUMERODESERIE" => " 0203081",
        "DESCRIPCIONFACTURA_1" => "SKU 0203081 FUENTE DE IMPRESORA ATM ITAUTEC CX3",
        "CLAVEPRODUCTOSERVICIO" => "EQUIPO",
        "CLAVEPRODUCTOSAT" => "72153700",
        "CLAVEPRODUCTODESCRIPCION" => "Servicios de instalación, mantenimiento",
        "NUMERODEIDENTIFICACION" => "47",
        "UNIDAD" => "SERVICIO",
        "VALORUNITARIO" => "1656",
        "cantidad" => "2",
        "Importedelimpuesto" => "0",
        "INCOTERM" => "DDP"
    ],
    [
        "DESCRIPCIONFACTURA" => "SKU 0203132 TARJETA  DE IMPRESORA ATM ITAUTEC CX3",
        "NUMERODESERIE" => " 0203132",
        "DESCRIPCIONFACTURA_1" => "SKU 0203132 TARJETA  DE IMPRESORA ATM ITAUTEC CX3",
        "CLAVEPRODUCTOSERVICIO" => "EQUIPO",
        "CLAVEPRODUCTOSAT" => "72153700",
        "CLAVEPRODUCTODESCRIPCION" => "Servicios de instalación, mantenimiento",
        "NUMERODEIDENTIFICACION" => "47",
        "UNIDAD" => "SERVICIO",
        "VALORUNITARIO" => "2280",
        "cantidad" => "2",
        "Importedelimpuesto" => "0",
        "INCOTERM" => "DDP"
    ],
    [
        "DESCRIPCIONFACTURA" => "SKU 0205069 IMPRESORA ATM ITAUTEC CX3",
        "NUMERODESERIE" => " 0205069",
        "DESCRIPCIONFACTURA_1" => "SKU 0205069 IMPRESORA ATM ITAUTEC CX3",
        "CLAVEPRODUCTOSERVICIO" => "EQUIPO",
        "CLAVEPRODUCTOSAT" => "72153700",
        "CLAVEPRODUCTODESCRIPCION" => "Servicios de instalación, mantenimiento",
        "NUMERODEIDENTIFICACION" => "47",
        "UNIDAD" => "SERVICIO",
        "VALORUNITARIO" => "6800",
        "cantidad" => "2",
        "Importedelimpuesto" => "0",
        "INCOTERM" => "DDP"
    ]
];

$certificado = new \CfdiUtils\Certificado\Certificado('sellos/00001000000503977924.cer');
$comprobanteAtributos = [
    'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
    'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
    'LugarExpedicion' => '03330',
    'MetodoPago' => 'PUE',
    'TipoDeComprobante' => 'I',
    'Total' => '76672.00',
    'TipoCambio' => 1,
    'Moneda' => 'USD',
    'SubTotal' => '76672.00',
    'CondicionesDePago' => 'INMEDIATO',
    'FormaPago' => '99',
    'Fecha' => '2022-12-07T16:00:00',
    'Folio' => '19426',
    'Serie' => 'I',
    'Version' => '3.3',
    'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
];

$creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);

// echo "comienza local";
// echo "<br>";
$myLocalResourcePath = 'tmp/';
$myResolver = new \CfdiUtils\XmlResolver\XmlResolver($myLocalResourcePath);
// ponerlo utilizando setXmlResolver
$cfdiCreator = new \CfdiUtils\CfdiCreator33();
$cfdiCreator->setXmlResolver($myResolver);

// ponerlo utilizando el constructor
$cfdiValidator = new \CfdiUtils\CfdiValidator33($myResolver);
// echo "termina generador local";
// echo "<br>";
$comprobante = $creator->comprobante();

// $relacion = $comprobante->addCfdiRelacionados([
//     'TipoRelacion' => '04'
// ]);

// $relacion->multiCfdiRelacionado([
//     ['UUID' => '9C97D32A-7D9B-442B-9CDE-612F2AEB0B04'],
//     ['UUID' => '23C3682F-AFA0-4F6A-8FE7-2CD69A714F14'],
// ]);

$comprobante->addEmisor([
    'RegimenFiscal' => '601',
    'Nombre' => 'Operadora Central de Estacionamientos, SAPI DE C.V.',
    'Rfc' => 'OCE9412073L3',
]);

$comprobante->addReceptor([
    'UsoCFDI' => 'P01',
    'Nombre' => 'OPERADORA CENTRAL DE ESTACIONAMIENTOS DE HONDURAS SA',
    'Rfc' => 'XEXX010101000',
]);
var_dump("comprobante: ", $comprobante);

foreach ($jayParsedAry as $item) {
    echo "importe: ";
    // var_dump($item['importe']);
    $concepto = $comprobante->addConcepto([
        'ClaveProdServ' => '72153700',
        'Cantidad' => $item['cantidad'],
        'ClaveUnidad' => 'H87',
        'Unidad' => $item['UNIDAD'],
        'Descripcion' => $item['DESCRIPCIONFACTURA'],
        'ValorUnitario' => $item['VALORUNITARIO'],
        'Importe' => ($item['VALORUNITARIO'] * $item['cantidad']),
        'NoIdentificacion' => 'M',
    ]);

    $concepto->addTraslado([
        'Importe' => ($item['VALORUNITARIO'] * $item['cantidad']),
        'TasaOCuota' => '0.000000',
        'TipoFactor' => 'Exento',
        'Impuesto' => '002',
        'Base' => ($item['VALORUNITARIO'] * $item['cantidad']),
    ]);

    // $concepto->addInformacionAduanera([
    //     // 'NumeroPedimento' => '19  47  3709  9002667',
    //     'Incoterm' => 'DDP',
    // ]);
}

// echo "Pedimento";
// //Ejemplo
// echo "<br>";
// $micadena = "ésta cadeña  tiene . Símbolõs";

// $micadena = eliminar_simbolos($micadena);

// echo $micadena;

/*foreach ($res as $item) {
    if ($item['SERIE'] == 'SN') {
        $comprobante->addConcepto([
            'ClaveProdServ' => $item['CLAVE_PRODUCTO'],
            'Cantidad' => $item['CANTIDAD'],
            'ClaveUnidad' => $item['CLAVE_UNIDAD'],
            'Descripcion' => eliminar_simbolos($item['DESCRIPCION']),
            'ValorUnitario' => $item['VALOR_UNITARIO'],
            'Importe' => $item['IMPORTE'],
        ])->addTraslado([
            'Importe' => str_replace(",", "", number_format(($item['IMPORTE'] * .16), 2)),
            'TasaOCuota' => '0.160000',
            'TipoFactor' => 'Tasa',
            'Impuesto' => '002',
            'Base' => str_replace(",", "", number_format($item['IMPORTE'], 2)),
        ]);
    } else {
        $comprobante->addConcepto([
            'ClaveProdServ' => $item['CLAVE_PRODUCTO'],
            'Cantidad' => $item['CANTIDAD'],
            'ClaveUnidad' => $item['CLAVE_UNIDAD'],
            'Descripcion' => $item['DESCRIPCION'],
            'ValorUnitario' => $item['VALOR_UNITARIO'],
            'Importe' => $item['IMPORTE'],
            'NoIdentificacion' => $item['SERIE']
        ])->addTraslado([
            'Importe' => str_replace(",", "", number_format(($item['IMPORTE'] * .16), 2)),
            'TasaOCuota' => '0.160000',
            'TipoFactor' => 'Tasa',
            'Impuesto' => '002',
            'Base' => str_replace(",", "", number_format($item['IMPORTE'], 2)),
        ]);

    }

}
*/


echo "<br>";

echo "<h2>Termina foreach</h2>";
$creator->addSumasConceptos(NULL, 2);
echo "<h1>Paso 1</h1>";
$key = file_get_contents('sellos/CSD_UNIDAD_OCE9412073L3_20200514_111724.key.pem');
echo "<h1>Paso 2</h1>" . $key;
libxml_disable_entity_loader(false);
$creator->addSello($key, 'OCE94120');
libxml_disable_entity_loader(true);
echo "<h1>Paso 3</h1>";
$creator->saveXml('facturaomar.xml');
echo "<h1>Paso 4</h1>";
$xml = $creator->asXml();
echo "<h1>Paso</h1>";
$xml2 = base64_encode($xml);
echo "<h1>Paso</h1>";
$body = array(
    'xml' => $xml2,
);
echo "<br>";
echo "guardo la factura sin timbrar $xml2";
echo "<h1>Comienza consumo</h1>";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api-stage.emite.dev/v1/stamp", //desarrollo
    //CURLOPT_URL => "https://api.emite.app/v1/stamp",//produccion
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => json_encode($body),
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json; charset=UTF-8",
        "Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM", //desarrollo
        //"Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI",//produccion
        "Content-Type: application/json; charset=UTF-8"
    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
if ($err) {
    return $err;
} else {
    $val = json_decode($response);
    echo $response;
}
if ($creator) {
    echo "<h3>Se creo archivo</h3>";
} else {
    echo "<h1>Fallo al crear</h1>";
}

$xml3 = $val->data->document_info->xml;
var_dump($xml3);
echo "<br>";
$uuid = $val->data->stamp_info->uuid;
$xml3 = base64_decode($xml3);
var_dump("Factura decodificada: ", $xml3);
//file_put_contents($uuid . ".txt", $xml3);
file_put_contents($uuid . ".xml", $xml3);

$cfdifile = $uuid . ".xml";
$xml = file_get_contents($cfdifile);
// clean cfdi
$xml = \CfdiUtils\Cleaner\Cleaner::staticClean($xml);
// create the main node structure
$comprobante = \CfdiUtils\Nodes\XmlNodeUtils::nodeFromXmlString($xml);
// create the main node structure
//$comprobante = \CfdiUtils\Nodes\XmlNodeUtils::nodeFromXmlString($xml);

echo "<h1>$cfdifile</h1>";
